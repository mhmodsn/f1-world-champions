# F1 world champions

This Angular application presents a list that shows the F1 world champions starting from 2005 until 2015

## Development server

Run `npm start` or `ng serve` or `yarn start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm build` or `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `npm test` or `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Profiles

By default, the application will use the `dev` profile. Located in `src/enviroments/environment.ts` and the production profile in `src/enviroments/environment.prod.ts`
Also the application configuration in these files, so you can configure season `start` to `end` and also the `API Url`.

## Caching support

The caching is enabled by default in the Local Storage because these data are historical, via [ngx-webstorage](https://github.com/PillowPillow/ng2-webstorage).

## Structure Tree

```
.
├── src
│   ├── app
│   │   ├── app.component.css
│   │   ├── app.component.html
│   │   ├── app.component.spec.ts
│   │   ├── app.component.ts
│   │   ├── app.module.ts
|   |   |
│   │   ├── circuit
│   │   │   └── circuit.model.ts
|   |   |
│   │   ├── constructor
│   │   │   └── constructor.model.ts
|   |   |
│   │   ├── driver
│   │   │   ├── driver.model.ts
│   │   │   ├── driver.service.spec.ts
│   │   │   └── driver.service.ts
|   |   |
│   │   ├── race
│   │   │   ├── race.model.ts
│   │   │   ├── race.service.spec.ts
│   │   │   ├── race.service.ts
│   │   │   └── result-list
│   │   │       ├── result-list.component.css
│   │   │       ├── result-list.component.html
│   │   │       ├── result-list.component.spec.ts
│   │   │       └── result-list.component.ts
|   |   |
│   │   └── season
│   │       └── season-list
│   │           ├── season-list.component.css
│   │           ├── season-list.component.html
│   │           ├── season-list.component.spec.ts
│   │           └── season-list.component.ts
|   |
│   ├── environments
│   │   ├── environment.prod.ts
│   │   └── environment.ts
```
