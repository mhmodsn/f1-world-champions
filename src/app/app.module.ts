import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {Ng2Webstorage} from 'ngx-webstorage';

import {AppComponent} from './app.component';
import {SeasonListComponent} from './season/season-list/season-list.component';
import {ResultListComponent} from './race/result-list/result-list.component';

@NgModule({
  declarations: [
    AppComponent,
    SeasonListComponent,
    ResultListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    Ng2Webstorage
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
