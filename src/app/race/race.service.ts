import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import {environment} from '../../environments/environment';
import {RaceResult} from './race.model';
import {LocalStorageService} from 'ngx-webstorage';

@Injectable()
export class RaceService {

  constructor(
    private http: Http,
    private localStorage: LocalStorageService
  ) { }

  getWinners(season: number, championId: string, forceRequest?: boolean): Observable<RaceResult> {
    const cached = this.getFromCache(season);

    return (!cached || forceRequest || season === this.getCurrentYear())

         ? this.http.get(this.winnersUrl(season))
               .map(res => res.json().MRData as RaceResult)
               .map(result => {
                   this.markSeasonChampionInEachRace(championId, result);
                   this.storeInCache(season, result);
                   return result;
               })

         : Observable.of(cached);

  }

  winnersUrl(season: number): string {
    return `${environment.api_url}/${season}/results/1.json`;
  }

  getFromCache(season: number): RaceResult {
    return this.localStorage.retrieve(`winners-${season}`) as RaceResult;
  }

  storeInCache(season: number, result: RaceResult) {
    this.localStorage.store(`winners-${season}`, result);
  }

  markSeasonChampionInEachRace(championId: string, result: RaceResult) {
    result.RaceTable.Races.forEach(race => race.seasonChampion = (race.Results[0].Driver.driverId === championId));
  }

  getCurrentYear(): number {
    return (new Date()).getFullYear();
  }
}
