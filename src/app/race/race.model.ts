import {Constructor} from '../constructor/constructor.model';
import {Driver} from '../driver/driver.model';
import {Circuit} from '../circuit/circuit.model';

export interface Time {
  millis?: string;
  time?: string;
}

export interface AverageSpeed {
  units?: string;
  speed?: string;
}

export interface FastestLap {
  rank?: string;
  lap?: string;
  Time?: Time;
  AverageSpeed?: AverageSpeed;
}

export interface Result {
  number?: string;
  position?: string;
  positionText?: string;
  points?: string;
  Driver?: Driver;
  Constructor?: Constructor;
  grid?: string;
  laps?: string;
  status?: string;
  Time?: Time;
  FastestLap?: FastestLap;
}

export interface Race {
  season?: string;
  round?: string;
  url?: string;
  raceName?: string;
  Circuit?: Circuit;
  date?: string;
  time?: string;
  Results?: Result[];
  seasonChampion?: boolean;
}

export interface RaceTable {
  season?: string;
  position?: string;
  Races?: Race[];
}

export interface RaceResult {
  xmlns?: string;
  series?: string;
  url?: string;
  limit?: string;
  offset?: string;
  total?: string;
  RaceTable?: RaceTable;
}
