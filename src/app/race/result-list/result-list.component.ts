import {Component, Input, OnInit, OnChanges, SimpleChanges} from '@angular/core';
import {RaceService} from '../race.service';
import {DriverService} from '../../driver/driver.service';
import 'rxjs/add/operator/switchMap';
import {RaceResult} from '../race.model';
import {DriverStanding, Standing} from '../../driver/driver.model';

const champion = {
  StandingsTable: {
    StandingsLists: [{
      DriverStandings: [{
        wins: '7',
        points: '300',
        Driver: {givenName: 'mahmoud', familyName: 'saeed', driverId: 'mahmoud'}
      }]
    }]
  }} as DriverStanding;

@Component({
  selector: 'app-result-list',
  templateUrl: './result-list.component.html',
  styleUrls: ['./result-list.component.css'],
  providers: [RaceService, DriverService]
})
export class ResultListComponent implements OnChanges {

  @Input()
  season: number;

  raceResult: RaceResult;
  champion: DriverStanding;

  constructor(
      private raceService: RaceService,
      private driverService: DriverService
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    this.driverService.getChampion(this.season)
        .switchMap(res => {
            this.setChampion(res);
            return this.raceService.getWinners(this.season, this.getChampionId());
        }).subscribe(data => this.raceResult = data);
  }

  setChampion(res: Standing) {
    this.champion = res.StandingsTable.StandingsLists[0].DriverStandings[0];
  }

  getChampionId(): string {
    return this.champion.Driver.driverId;
  }

}
