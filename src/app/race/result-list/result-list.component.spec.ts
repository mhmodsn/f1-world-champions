import {async, ComponentFixture, TestBed, tick, fakeAsync} from '@angular/core/testing';

import {ResultListComponent} from './result-list.component';
import {HttpModule} from '@angular/http';
import {Ng2Webstorage} from 'ngx-webstorage';
import {DebugElement} from '@angular/core';
import {RaceService} from '../race.service';
import {DriverService} from '../../driver/driver.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {By} from '@angular/platform-browser';

describe('ResultListComponent', () => {
  let comp: ResultListComponent;
  let fixture: ComponentFixture<ResultListComponent>;


  let spy: jasmine.Spy;
  let de: DebugElement;
  let el: HTMLElement;
  let raceService: RaceService;
  let driverService: DriverService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule,
        Ng2Webstorage
      ],
      declarations: [ ResultListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultListComponent);
    comp = fixture.componentInstance;

    raceService = fixture.debugElement.injector.get(RaceService);
    driverService = fixture.debugElement.injector.get(DriverService);

    const winner = { RaceTable: { Races: [{
              round: '1',
              url: 'http://f1.com',
              raceName: 'f1 race',
              Circuit: {
                url: 'http://circuit.com',
                circuitName: 'agour el-raml',
                Location: {country: 'egypt', city: 'quesna', lat: '20.78478', long: '-25.77778'}
              },
              date: '2017-10-6',
              Results: [{
                points: 25,
                Driver: {url: 'http://driver.com', givenName: 'mahmoud', familyName: 'saeed', driverId: 'mahmoud'},
                Constructor: {url: 'http://constructor.com', name: 'nasr'},
                Time: {time: '10.10.10'},
                laps: 50
              }],
              seasonChampion: true
            }]}};


    const champion = {
      StandingsTable: {
        StandingsLists: [{
          DriverStandings: [{
            wins: '7',
            points: '300',
            Driver: {givenName: 'mahmoud', familyName: 'saeed', driverId: 'mahmoud'}
          }]
        }]
      }};

    spy = spyOn(raceService, 'getWinners').and.returnValue(Observable.of(winner));
    spy = spyOn(driverService, 'getChampion').and.returnValue(Observable.of(champion));

    de = fixture.debugElement.query(By.css('.races'));
    el = de.nativeElement;
  });

  it('should be created', () => {
    expect(comp).toBeTruthy();
  });

  it('should not show any race before OnInit', () => {
    expect(el.textContent.trim()).toBe('', 'nothing displayed');
    expect(spy.calls.any()).toBe(false, 'getWinners & getChampion not yet called');
  });

  it('should show winner after getWinners (fakeAsync)', fakeAsync(() => {
    fixture.detectChanges();
    tick();
    fixture.detectChanges();
    expect(el.innerText).toContain('mahmoud');
  }));

});
