import {fakeAsync, inject, TestBed, tick} from '@angular/core/testing';

import {DriverService} from './driver.service';
import {HttpModule, RequestMethod, ResponseOptions, XHRBackend, Response} from '@angular/http';
import {MockBackend} from '@angular/http/testing';
import {Ng2Webstorage} from 'ngx-webstorage';

describe('DriverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule,
        Ng2Webstorage
      ],
      providers: [
        DriverService,
        {provide: XHRBackend, useClass: MockBackend}
      ]
    });
  });


  it('should get the champion of the season', fakeAsync(
    inject([DriverService, XHRBackend], (service, mockBackend) => {

      const season = 2008;
      const expectedUrl = 'http://ergast.com/api/f1/2008/driverStandings/1.json';
      const mockResponse = {
        MRData: {
          StandingsTable: {
            StandingsLists: [{
              DriverStandings: [{
                Driver: {
                  driverId: 'hamilton'
                }
              }]
            }]
        }}};

      mockBackend.connections.subscribe((connection) => {
          expect(connection.request.method).toBe(RequestMethod.Get);
          expect(connection.request.url).toBe(expectedUrl);

          connection.mockRespond(new Response(
                        new ResponseOptions({ body: JSON.stringify(mockResponse) })
                    ));
        });

      service.getChampion(season, true).subscribe(standing => {
        const expectedId = 'hamilton';
        const driverId = standing.StandingsTable.StandingsLists[0].DriverStandings[0].Driver.driverId;

        expect(driverId).toBe(expectedId);
      });

      service.clearCache(season);
      expect(service.getFromCache(season)).toBeNull();
    })
  ));


  it('should be created',
    inject([DriverService], (service: DriverService) => {

      expect(service).toBeTruthy();
  }));


  it('should return the driver champion url',
    inject([DriverService], (service: DriverService) => {

      const expectedUrl = 'http://ergast.com/api/f1/2008/driverStandings/1.json';
      expect(service.championUrl(2008)).toBe(expectedUrl);
  }));


  it('should return the current year',
    inject([DriverService], (service: DriverService) => {

      expect(service.getCurrentYear()).toBe(2017);
  }));


  it('should keep the driver standing in the cache',
    inject([DriverService], (service: DriverService) => {

      const season = 2020;
      const standingMock = {
        xmlns: '...',
        series: '...',
        url: '...',
        limit: '...',
        offset: '...',
        total: '...',
        StandingsTable: null
      };

      service.storeInCache(season, standingMock);

      expect(service.getFromCache(season)).toEqual(standingMock);
  }));


  it('should update the driver standing in the cache',
    inject([DriverService], (service: DriverService) => {

      const season = 2020;
      const standingMock = {
        xmlns: '...',
        series: '000',
        url: '...',
        limit: '0',
        offset: '0',
        total: '...',
        StandingsTable: null
      };

      service.storeInCache(season, standingMock);

      expect(service.getFromCache(season)).toEqual(standingMock);
  }));


  it('should clear the driver standing in the cache',
    inject([DriverService], (service: DriverService) => {
      const season = 2020;

      service.clearCache(season);
      expect(service.getFromCache(season)).toBeNull();
    }));

});
