import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import {environment} from '../../environments/environment';
import {Standing} from './driver.model';
import {LocalStorageService} from 'ngx-webstorage';

@Injectable()
export class DriverService {

  constructor(
    private http: Http,
    private localStorage: LocalStorageService
  ) { }

  getChampion(season: number, forceRequest?: boolean): Observable<Standing> {
    const cached = this.getFromCache(season);

    return (!cached || forceRequest || season === this.getCurrentYear())

         ? this.http.get(this.championUrl(season))
               .map(res => {
                   const standing = res.json().MRData as Standing;
                   this.storeInCache(season, standing);
                   return standing;
               })

         : Observable.of(cached);
  }

  championUrl(season: number): string {
    return `${environment.api_url}/${season}/driverStandings/1.json`;
  }

  getFromCache(season: number): Standing {
    return this.localStorage.retrieve(`champion-${season}`) as Standing;
  }

  storeInCache(season: number, standing: Standing) {
    this.localStorage.store(`champion-${season}`, standing);
  }

  getCurrentYear(): number {
    return (new Date()).getFullYear();
  }

  clearCache(season: number) {
    this.localStorage.clear(`champion-${season}`);
  }

}
