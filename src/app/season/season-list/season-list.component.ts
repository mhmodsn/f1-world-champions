import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-season-list',
  templateUrl: './season-list.component.html',
  styleUrls: ['./season-list.component.css']
})
export class SeasonListComponent implements OnInit {

  seasons: number[] = [];

  @Input()
  season: number;

  @Output()
  onChanged = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
    this.setSeasons();
  }

  setSeasons() {
    for (let i = environment.season.start; i <= environment.season.end; i++) {
      this.seasons.push(i);
    }
    this.seasons.reverse();
  }

  onChange(season: number) {
    this.onChanged.emit(season);
  }

}
