export const environment = {
  production: true,
  season: {
    start: 2005,
    end:   2015
  },
  api_url: 'http://ergast.com/api/f1'
};
